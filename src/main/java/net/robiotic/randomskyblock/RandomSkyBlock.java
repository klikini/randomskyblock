package net.robiotic.randomskyblock;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public final class RandomSkyBlock extends JavaPlugin {
    // Configuration
    private boolean giveItems;
    private long interval;
    private boolean sound;

    // Internal state
    private ItemRandomizer itemRandomizer;

    /**
     * Update the internal state of the plugin when its server state or options change.
     */
    private void update() {
        this.saveConfig();

        if (itemRandomizer != null) {
            itemRandomizer.cancel();
            itemRandomizer = null;
        }

        if (isEnabled() && giveItems) {
            itemRandomizer = new ItemRandomizer(this, sound);
            itemRandomizer.runTaskTimer(this, 0, interval * 20);
        }
    }

    @Override
    public void onLoad() {
        this.saveDefaultConfig();
        this.giveItems = this.getConfig().getBoolean("enabled");
        this.interval = this.getConfig().getLong("interval");
        this.sound = this.getConfig().getBoolean("sound");
    }

    @Override
    public void onEnable() {
        saveDefaultConfig();
        update();
    }

    @Override
    public void onDisable() {
        update();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("rsb")) {
            if (args.length == 0) {
                sender.sendMessage("RandomSkyBlock items are currently " + (giveItems ? "enabled" : "disabled") + ".");
                return true;
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("on")) {
                    // Enable random items
                    giveItems = true;
                    Bukkit.getServer().broadcastMessage("RandomSkyBlock items enabled.");
                    this.getConfig().set("enabled", giveItems);
                    update();
                    return true;
                } else if (args[0].equalsIgnoreCase("off")) {
                    // Disable random items
                    giveItems = false;
                    Bukkit.getServer().broadcastMessage("RandomSkyBlock items disabled.");
                    this.getConfig().set("enabled", giveItems);
                    update();
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else if (cmd.getName().equalsIgnoreCase("rsb_interval")) {
            if (args.length == 0) {
                final String message = String.format("Current interval: %d second%s.",
                        interval, interval == 1 ? "" : "s");
                sender.sendMessage(message);

                return true;
            } else if (args.length == 1) {
                try {
                    interval = Integer.parseInt(args[0]);
                } catch (NumberFormatException exception) {
                    sender.sendMessage("Invalid interval: " + exception.getMessage());
                    return false;
                }

                if (interval < 0) {
                    sender.sendMessage("Interval must be positive.");
                    return false;
                }

                final String message = String.format("Random item interval changed to %d second%s.",
                        interval, interval == 1 ? "" : "s");
                getServer().broadcastMessage(message);

                this.getConfig().set("interval", interval);
                update();
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
